var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var htmlreplace = require('gulp-html-replace');
var source = require('vinyl-source-stream');
var browserify = require('browserify');
var watchify = require('watchify');
var reactify = require('reactify');
var streamify = require('gulp-streamify');
var jshint = require('gulp-jshint');
var cssminify = require('gulp-minify-css');
var imagemin = require('gulp-imagemin');
var connect = require('gulp-connect'); 
var livereload = require('gulp-livereload');
var clean = require('gulp-clean');
var sourcemaps = require('gulp-sourcemaps');
var notify = require('gulp-notify');

var path = {
  HTML: 'src/index.html',
  MINIFIED_OUT: 'build.min.js',
  OUT: 'build.js',
  DEST: 'dist',
  DEST_BUILD: 'dist/build',
  DEST_SRC: 'dist/src',
  ENTRY_POINT: './src/js/App.js',
	PORT_DEV: 8888,
	PORT_BUILD: 8889,
};

/*
// Server Task
gulp.task('server', function() {
 connect.server({
     root: path.DEST_SRC,
		 port: path.PORT,
     livereload: true
   });
});

gulp.task('copy', function(){
  gulp.src(path.HTML)
    .pipe(gulp.dest(path.DEST_SRC))
		.pipe(notify({ message: 'Html task complete' }));
});

gulp.task('watch', function() {
  gulp.watch(path.HTML, ['replaceSrcHTML']);
	var watcher  = watchify(browserify({
    entries: [path.ENTRY_POINT],
    transform: [reactify],
    debug: true,
    cache: {}, packageCache: {}, fullPaths: true
  }));
	return watcher.on('update', function () {
    watcher.bundle()
      .pipe(source(path.OUT))
      .pipe(gulp.dest(path.DEST_SRC + '/js/'))
      console.log('Updated');
  })
    .bundle()
    .pipe(source(path.OUT))
    .pipe(gulp.dest(path.DEST_SRC + '/js/'));
});

gulp.task('build', function(){
  browserify({
    entries: [path.ENTRY_POINT],
    transform: [reactify],
  })
    .bundle()
    .pipe(source(path.MINIFIED_OUT))
    .pipe(streamify(uglify(path.MINIFIED_OUT)))
    .pipe(gulp.dest(path.DEST_BUILD + '/js/'));
});

gulp.task('replaceHTML', function(){
  gulp.src(path.HTML)
    .pipe(htmlreplace({
      'js': 'js/' + path.MINIFIED_OUT
    }))
    .pipe(gulp.dest(path.DEST_BUILD));
});

gulp.task('replaceSrcHTML', function(){
  gulp.src(path.HTML)
    .pipe(htmlreplace({
      'js': 'js/' + path.OUT
    }))
    .pipe(gulp.dest(path.DEST_SRC));
});

gulp.task('production', ['replaceHTML', 'build']);
gulp.task('default', ['server', 'watch']);

*/
// Error Helper
function onError(err) {
    console.log(err);
}
 
// Server Task
gulp.task('server', function() {
 connect.server({
     root: 'dist/build',
		 port: path.PORT_BUILD,
     livereload: true
   });
 connect.server({
     root: 'dist/src',
		 port: path.PORT_DEV,
     livereload: true
   });
});
 
// Styles Task
gulp.task('styles', function() {
	gulp.src('src/css/*.css')
		.pipe(sourcemaps.init())
		.pipe(concat('all.min.css'))
		.pipe(cssminify())  
		.pipe(sourcemaps.write())      
		.pipe(gulp.dest(path.DEST_BUILD + '/css'))
		.pipe(notify({ message: 'Styles task complete' }));
	gulp.src('src/css/*.css')
		.pipe(sourcemaps.init())
		//.pipe(concat('all.min.css'))
		//.pipe(cssminify())  
		.pipe(sourcemaps.write())      
		.pipe(gulp.dest(path.DEST_SRC + '/css'))
		.pipe(notify({ message: 'Styles task complete' }));
});
 
gulp.task('html', function () {
  gulp.src('src/*.html')
		.pipe(htmlreplace({
				'js': 'js/' + path.MINIFIED_OUT
			}))
    .pipe(gulp.dest(path.DEST_BUILD))
		.pipe(notify({ message: 'Html task complete' }));
  gulp.src('src/*.html')
		.pipe(htmlreplace({
				'js': 'js/' + path.OUT
			}))
    .pipe(gulp.dest(path.DEST_SRC))
		.pipe(notify({ message: 'Html task complete' }));
});
 
// Scripts Task
gulp.task('scripts', function() {
		browserify({
			entries: [path.ENTRY_POINT],
			transform: [reactify],
		})
			.bundle()
			.pipe(source(path.MINIFIED_OUT))
			.pipe(streamify(uglify(path.MINIFIED_OUT)))
			.pipe(gulp.dest(path.DEST_BUILD + '/js/'))
		 	.pipe(notify({ message: 'Scripts task complete' }));
		browserify({
			entries: [path.ENTRY_POINT],
			transform: [reactify],
		})
			.bundle()
			.pipe(source(path.OUT))
			//.pipe(streamify(uglify(path.MINIFIED_OUT)))
			.pipe(gulp.dest(path.DEST_SRC + '/js/'))
		 	.pipe(notify({ message: 'Scripts task complete' }));
    // return gulp.src('src/js/*.js')
		// 	.pipe(sourcemaps.init())
		// 	.pipe(jshint())
		// 	.pipe(jshint.reporter('default'))
		// 	.pipe(concat('all.min.js'))
		// 	.pipe(uglify())
		// 	.pipe(sourcemaps.write())
		// 	.pipe(gulp.dest('dist/build/js'))
		// 	.pipe(notify({ message: 'Styles task complete' }));
});
 
// Images Task
gulp.task('images', function() {
	gulp.src('src/img/*')
		.pipe(imagemin())
		.pipe(gulp.dest(path.DEST_BUILD + '/img'))
		.pipe(notify({ message: 'Images task complete' }));
	gulp.src('src/img/*')
		.pipe(imagemin())
		.pipe(gulp.dest(path.DEST_SRC + '/img'))
		.pipe(notify({ message: 'Images task complete' }));
});
 
 
// Clean Task
gulp.task('clean', function() {
  gulp.src([path.DEST_BUILD + '/css/*', path.DEST_BUILD + '/js/*', path.DEST_BUILD + '/img/*'], {read: false})
		.pipe(clean());
  gulp.src([path.DEST_SRC + '/css/*', path.DEST_SRC + '/js/*', path.DEST_SRC + '/img/*'], {read: false})
		.pipe(clean());
});
 
// Watch Task
gulp.task('watch', function() {
    gulp.watch('src/*.html', ['html']);
    gulp.watch('src/css/*.css', ['styles']);
    gulp.watch(['src/js/*.js', 'src/js/**/*.js'], ['scripts']);
    gulp.watch('src/img/*', ['images']);

		var watcher  = watchify(browserify({
			entries: [path.ENTRY_POINT],
			transform: [reactify],
			debug: true,
			cache: {}, packageCache: {}, fullPaths: true
		}));
 
    // Watch any files in server/, reload on change
    livereload.listen();
    gulp.watch([path.DEST_BUILD + '/**', path.DEST_SRC + '/**']).on('change', livereload.changed);
});

// Default Task
gulp.task('default', ['clean', 'html', 'styles', 'scripts', 
                     'images', 'server', 'watch']);