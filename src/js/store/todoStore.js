var appDispatcher = require('../dispatcher/appDispatcher');
var appConstants = require('../constant/appConstants');
//var objectAssign = require('react/lib/Object-assign');
var objectAssign = require('object-assign');
var EventEmitter = require('events').EventEmitter;

var CHANGE_EVENT = 'change';

var _store = {
	list: ['123', 'kkkk'],
};

var addItem = function(data) {
	_store.list.push(data);
};

var removeItem = function(data) {
	_store.list.splice(data, 1);
};

var todoStore = objectAssign({}, EventEmitter.prototype, {
	addChangeListener: function(cb) {
		this.on(CHANGE_EVENT, cb);
	},

	removeChangeListener: function(cb) {
		this.removeListener(CHANGE_EVENT, cb);
	},

	getList: function() {
		return _store.list;
	},
})

appDispatcher.register(function(payload) {
	var action = payload.action;
	switch (action.actionType) {
		case appConstants.ADD_ITEM:
			addItem(action.data);
			todoStore.emit(CHANGE_EVENT);
			break;
		case appConstants.REMOVE_ITEM:
			removeItem(action.data);
			todoStore.emit(CHANGE_EVENT);
			break;
		default:
			return true;
	}
});

module.exports = todoStore;