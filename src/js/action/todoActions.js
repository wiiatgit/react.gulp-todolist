var AppDispatcher = require('../dispatcher/appDispatcher');
var appConstants = require('../constant/appConstants');

var todoActions = {
	addItem: function (data) {
		AppDispatcher.handleAction({
			actionType: appConstants.ADD_ITEM,
			data: data,
		});
	},

	removeItem: function (data) {
		AppDispatcher.handleAction({
			actionType: appConstants.REMOVE_ITEM,
			data: data,
		})
	},
}

module.exports = todoActions;