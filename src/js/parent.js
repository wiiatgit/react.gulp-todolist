var React = require('react');
var Child = require('./child');

var Parent = React.createClass({
  render: function(){
    return (
      <div>
        <div> This is the parent. </div>
        <Child name="fff">KDKFL</Child>
      </div>
    )
  }
});

module.exports = Parent;