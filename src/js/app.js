var React = require('react');
var ReactDOM = require('react-dom');
var ListContainer = require('./component/ListContainer');

var App = React.createClass({
  render: function(){
    return (
      <div className="container">
        Hello ToDo
        <div className="row">
          <ListContainer />
        </div>
      </div>
    )
  }
});

ReactDOM.render(
  <App />,
  document.getElementById('app')
)