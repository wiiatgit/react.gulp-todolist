var React = require('react');
var AddItem = require('./AddItem');
var List = require('./List');
var todoStore = require('../store/todoStore');
var todoActions = require('../action/todoActions');

var ListContainer = React.createClass({
	getInitialState: function() {
		return {
			list: todoStore.getList()
		}
	},

	componentWillMount: function() {

	},

	componentDidMount: function() {
		todoStore.addChangeListener(this._onChange);
	},

	componentWillUnmount: function() {
		todoStore.removeChangeListener(this._onChange);
	},

	_onChange: function() {
		this.setState({
			list: todoStore.getList()
		});
	},

	handleAddItem: function(data) {
		todoActions.addItem(data);
	},

	handleRemoveItem: function(data) {
		todoActions.removeItem(data);
	},

	render: function() {
		return (
			<div className="col-sm-6 col-md-offset-3">
        <div className="col-sm-12">
          <h3 className="text-center"> Todo List </h3>
          <AddItem add={this.handleAddItem}/>
          <List items={this.state.list} remove={this.handleRemoveItem}/>
        </div>
      </div>
		)
	},
})

module.exports = ListContainer;