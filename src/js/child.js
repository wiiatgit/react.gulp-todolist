var React = require('react');
var Child = React.createClass({
  render: function(){
    return (
      <div>
        and this is th e <b>{this.props.name}</b>.
        <br />{this.props.children}
      </div>
    )
  }
});

module.exports = Child;