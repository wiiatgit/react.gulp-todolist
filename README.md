# 初步設計 #
## 參考 ##
原版: https://tylermcginnis.com/react-js-tutorial-pt-2-building-react-applications-with-gulp-and-browserify-5489228dde99
中譯版: http://blog.csdn.net/mqy1023/article/details/51607820

## 加上 npm packages ##
```
#!shell

npm install --save gulp gulp-concat gulp-uglify gulp-react gulp-html-replace
npm install --save vinyl-source-stream browserify watchify reactify gulp-streamify
```

## 檔案結構 ##
針對原版裡的檔案結構, 把 dist 後的 src 和 build 的碼都分開, 所以資料結構變成
- dist
	- build
		- css
		- img
		- js
	- src
		- css
		- img
		- js
- src
	- css
	- img
	- js
gulpfile.js
package.json
README

## STEP 2 ##
再參考 https://blog.ccjeng.com/2015/08/Gulp.html
加上 jshint gulp-clean gulp-sourcemap gulp-connect gulp-livereload gulp-minify-css gulp-jshint gulp-imagemin gulp-notify 等套件

接著修改 gulp default 設定, 除了編譯成 dev 版之外, 一併編譯成 production 版本

# 改為 TODO APP #
##nodejs package##
```
#!batch
npm install --save flux react-dom object-assign
```
##檔案結構##
先在 src/js/ 目錄底下建立 4 個資料夾
- src
	- js
		- action
		- component
		- dispatcher
		- store
## 加入 View Component ##
### ListContainer ###
在 component 底下新增 ListContainer.js
```
#!nodejs
var React = require('react');
var AddItem = require('./AddItem');
var List = require('./List');
var todoStore = require('../store/todoStore');
var todoActions = require('../action/todoActions');

var ListContainer = React.createClass({
	getInitialState: function() {
		return {
			list: todoStore.getList()
		}
	},

	componentWillMount: function() {

	},

	componentDidMount: function() {
		todoStore.addChangeListener(this._onChange);
	},

	componentWillUnmount: function() {
		todoStore.removeChangeListener(this._onChange);
	},

	componentDidUnmount: function() {

	},

	_onChange: function() {
		this.setState({
			list: todoStore.getList()
		})
	},

	handleAddItem: function(data) {
		todoActions.addItem(data);
	},

	handleRemoveItem: function(data) {
		todoActions.removeItem(data);
	},

	render: function() {
		return (
			<div>
				<div>
					<h3>Todo List</h3>
					<AddItem add={this.handleAddItem} />
					<List items={this.state.list} remove={this.handleRemoveItem} />
				</div>
			</div>
		)
	},
})

module.exports = ListContainer;
```
###加入 List###
```
#!nodejs
TODO
```
###加入 AddItem###
```
#!nodejs
TODO
```
##加入 appConstants##
在 src/js/constant 底下加入 appConstants.js
```
#!nodejs
var appConstants = {
	ADD_ITEM: "ADD_ITEM",
	REMOVE_ITEM: "REMOVE_ITEM",
};

module.exports = appConstants;
```
##加入 appDispatcher##
在 src/js/dispatcher 底下加入 appDispatcher.js
```
#!nodejs
var Dispatcher = require('flux').Dispatcher;

var AppDispatcher = new Dispatcher();
AppDispatcher.handleAction = function(action) {
	this.dispatch({
		source: 'VIEW_ACTION',
		action: action,
	});
};

module.exports = AppDispatcher;
```
##加入 todoActions##
在 src/js/action 底下加入 todoActions.js
```
#!nodejs
TODO
```
##加入 todoStore##
在 src/js/store 底下加入 todoStore.js
```
#!nodejs
TODO
```

#說明#
View 去叫 Store 的 getList, 取得資料. 如果要變更資料, 就要透過 Action 去做. 也就是 Store 的 getList 方法是公開的, 而寫入資料的方法 (addItem, removeItem等等) 則是透過 Dispatcher 開放出去, 然後再呼叫 Store 的 addItem, removeItem 對資料做寫入動作

Action 會觸發 Dispatcher 裡的事件, Dispatcher 再去叫 Store 做事
因此 Store 裡面要對 Dispatcher 註冊 payload
這樣才能在事情發生時, 於 Store 裡面做 addItem/ removeItem 的動作

Store 本身要有 addChangeListener 和 removeChangeListener 這兩個東西, 才能讓 View 在載入時去註冊 listener, 那麼在 View 對資料做任何操作時, 就會先叫用 Action 的資料存取方法, 然後 Action 會讓 Dispatcher 去做事 (如上一段所述), 又因為 Store 針對 Dispatcher 做了 註冊 payload 的動作, 所以當 Dispatcher 做了什麼動作之後, Store 就會自動去呼叫 EventEmitter 去做事情. 而 EventEmitter 做的事就是傳給 addChangeListener 的 callback function.

因此, View 就會經由 callback function 取得最新的資料

#可能的問題#
* React 在0.14版之後做了不少修改, 其中 getDOMNode 已經被 findDOMNode 取代
參考 https://facebook.github.io/react/docs/more-about-refs.html
所以直接用 this.refs.REFNAME 就可以得到 DOM 物件
* 而 React.render 也被 ReactDOM.render 取代
* 不會觸發 addItem 及 removeItem
原來是因為在 todoActions 裡 require dispatcher 時, 大小寫錯了, 所以載不進去 dispatcher